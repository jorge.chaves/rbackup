# rBackup

## INDICE

    0  INTRODUCCION

    1  SINTAXIS GENERAL

      1.1  Comando sync
      1.2  Comando explain
      1.3  Comando run

    2  FORMATO DE LOS ARCHIVOS RBACKUP

      2.1  Variables
        2.1.1  Variables de usuario
        2.1.2  Variables intrinsecas

      2.2  Secciones
        2.2.1  Seccion [rbackup]
        2.2.2  Seccion [commands]
        2.2.3  Seccion [mail]

    3  COMMANDS

      3.1  Introduccion
      3.2  Comandos "title"
      3.3  Comandos "sync"
      3.4  Comandos "shell"

    4  FORMATO DE LOS ARCHIVOS SYNC

      4.1  Variables
        4.1.1  Variables intrinsecas y de usuario

      4.2  Secciones
        4.2.1  Seccion [paths]
        4.2.2  Seccion [source]
        4.2.3  Seccion [filters]
        4.2.4  Seccion [options]


-------------------
## 0 - INTRODUCCION
-------------------


Rbackup es un script que permite ejecutar de manera condicional comandos de
Linux declarados en un archivo.

Opcionalmente envia un resumen en formato HTML a un archivo y/o destinatarios de
correo con el resultado de la ejecucion de cada comando y el tiempo insumido por
cada uno.

El texto correspondiente a cada comando en el informe puede personalizarse para
ser amigable al usuario que lo lee.

Tambien permite configurar en un archivo procesos de sincronizacion a ejecutar
con rsync.

Está desarrollado para ser ejecutado en servidores Debian 7+ con python 2.7 sin
necesidad de instalar librerias adicionales.


-----------------------
## 1 - SINTAXIS GENERAL
-----------------------

> rbackup [-h] [--version] [-v | -d] COMANDO ...

    Argumentos opcionales:
      -h, --help          Muestra ayuda y sale
      --version           Muestra version del programa sale

    Salida de texto a consola:
      -v, --verbose       Muestra el texto y resultado de los comandos
      -d, --debug         Salida detallada para depurar del programa

    COMANDO:
        sync              Muestra/prueba/ejecuta un archivo sync
        explain           Explica el contenido de un archivo rbackup
        run               Lee un archivo rbackup y ejecuta sus comandos


### 1.1 - COMANDO SYNC

#### Muestra, ejecuta o prueba el comando rsync resultante de un archivo sync

> rbackup sync [-h] [--show | --test | --exec] file

    Argumentos posicionales:
      file             Archivo sync a procesar

    Argumentos opcionales:
      -h, --help       Muestra ayuda del comando y sale
      --extra EXTRA    Opciones extra para rsync, ej: '--archive --compress'

    Opciones de sync (mutuamente excluyentes):
      --show      [predeterminado] muestra el comando rsync sin ejecutarlo
      --exec      ejecuta el comando rsync
      --test      ejecuta el comando rsync en modo prueba (--dry-run)


### 1.2 - COMANDO EXPLAIN

#### Explica el contenido de un archivo rbackup

> rbackup explain [-h] file

    Argumentos posicionales:
      file             Archivo rbackup a interpretar

    Argumentos opcionales:
      -h, --help       Muestra ayuda del comando y sale


### 1.2 - COMANDO RUN

#### Ejecuta los comandos de un archivo rbackup

    rbackup run [-h] [-m | -M] file

    Argumentos posicionales:
      file             Archivo rbackup a ejecutar

    Argumentos opcionales:
      -h, --help       Muestra ayuda del comando y sale

    Opciones de mail (mutuamente excluyentes):
      -m, --send-mail       Envia resumen por mail
      -M, --send-errormail  Envia resumen por mail solo si el status es error


--------------------------------------
## 2 - FORMATO DE LOS ARCHIVOS RBACKUP
--------------------------------------

### 2.1 VARIABLES

#### 2.1.1 variables de usuario

Son variables declaradas por el usuario para evitar repeticion de codigo y/o
mejorar la legibilidad del archivo.
Estas variables se declaran al inicio, antes de declarar la primera seccion.
El formato es  "nombre: valor"
El nombre de la variable debe comenzar con una letra minuscula.
Luego las variables pueden utilizarse en el valor de cualquier opcion de la
siguiente forma "%nombre_de_variable%", donde rbackup las interpretara como
el valor asignado.

Ejemplo:

>       ruta: /home/user
>
>       [rbackup]
>       title: "Ejecucion de %ruta%/proceso1.sh"

En este caso el valor de title será "Ejecucion de /home/user/proceso1.sh"

#### 2.1.2 Variables intrinsecas

Son variables ya incluidas con el programa y se pueden utilizar de la misma
forma que las variables de usuario.
El nombre de estas variables siempre esta en mayusculas.
Las variables actualmente disponibles son:

        %DD%          Dia actual (dos digitos)
        %MM%          Mes actual (dos digitos)
        %YY%          Año actual (dos digitos)
        %YYYY%        Año actual (cuatro digitos)
        %DAY%         Dia actual (sin formato)
        %MONTH%       Mes actual (sin formato)
        %YEAR%        Año actual (sin formato)
        %HH%          Hora actual (dos digitos)
        %NN%          Minuto actual (dos digitos)
        %SS%          Segundo actual (dos digitos)
        %HOSTNAME%    Nombre del host actual
        %USER%        Nombre del usuario actual

### 2.2 - SECCIONES

#### 2.2.1 - Seccion [rbackup]

(opcional) Contiene la descripcion del rbackup y opciones globales

Items:

        title: titulo_a_mostrar_en_resumen          (default: archivo rbackup)
        description: descripcion_del_rbackup            (opcional)
        summary: archivo_donde_guardar_el_resumen_html  (opcional)

Notas:
- Esta disponible la variable $FILE en "title" y "description", que mostrara el
  path completo al archivo rbackup

#### 2.2.2 - Seccion [commands]

(requerida) Contiene la declaracion de los titulos, comandos y procesos sync

Ver formato mas adelante en "COMMANDS"

#### 2.2.3 - Seccion [mail]

(opcional) Contiene los datos de configuracion para enviar el resumen por correo

Items:

        from: direccion_de_mail_origen
        to: direcciones_de_mail_destinatarios (separados por "," o ";")
        host: direccion_servidor_smtp                       (default: localhost)
        port: puerto_servidor_smtp (2 a 65535)                  (default: 25)
        ssl: envio_cifrado  (valores admitidos: "yes", "no")    (default: no)
        login: usuario_para_login_smtp                          (default: "")
        passwd: password_login_smtp                             (default: "")
        timeout: segundos_de_espera_conexion_smtp (1 a 120)     (default: 30)
        subject : asunto del mail   (default: "rBackup [$RESULT]: $TITLE")
                  Variables disponibles para el valor de subject:
                  $RESULT: "OK" o "ERROR" segun resultado global
                  $TITLE: titulo declarado en rbackup
                  $DESCRIPTION: descripcion declarada en rbackup

---------------
## 3 - COMMANDS
---------------

### 3.1 - Introduccion

Son los comandos que se declaran en la seccion [commands] de archivos rbackup
Hay tres tipos de comandos: title, sync y shell y se ejecutan en el orden que
estan declarados en el archivo.
Se muestran en el resumen HTML los titulos y aquellos comandos que tienen un
texto a mostrar, el resto de los comandos se ejecutan pero no son mostrados en
el resumen.

Parametros opcionales comunes a todos los comandos:

        --ifok          Se ejecuta solo si el ultimo resultado fue OK
        --iferror       Se ejecuta solo si el ultimo resultado fue ERROR
        --ifokexit      Si el comando resulta OK finaliza todo el proceso
        --iferrorexit   Si el comando resulta ERROR finaliza todo el proceso
        --oktext        Texto a mostrar si el resultado del comando es OK
        --errortext     Texto a mostrar si el resultado del comando es ERROR
        --text          Texto predeterminado, el comando se mostrara siempre
        --backcolor     Color de fondo del comando o titulo
        --forecolor     Color del texto del comando o titulo
        --okcodes       Codigos de retorno considerados OK separados por coma
                        (default: 0)
        --okstatus      Texto de status OK. (default: "OK")
        --errorstatus   Texto de status ERROR. (default: "ERROR")

Notas:

- El texto en --oktext y --errortext tienen prioridad, --text se asigna como
  ultima opcion.
- Los colores son compatibles con la sintaxis de estilo HTML, ejemplos: #4e4e4e,
  green, etc.
- Tanto los titulos como los comandos tienen asignados colores de fondo y texto
  por defecto.
- En text, oktext y errortext se encuentra disponible la variable $COMMAND, que
  mostrara el comando a ejecutar.
- En los comandos sync tambien esta disponible la variable $FILE que mostrara el
  path completo al archivo sync. A partir de la version 3.4.0 el valor de text
  predeterminado para los comandos de tipo sync es "Sync: $FILE".
- En text, oktext y errortext se encuentra disponible la variable $EXITCODE, que
  mostrara el codigo de salida del comando ejecutado.

### 3.2 - Comandos "title"

Imprimen lineas de titulo en el resumen. Devuelven OK.

Sintaxis:

>        title {parametros opcionales} texto

Si el texto incluye espacios debe utilizarse comillas, ejemplo:

>       title "Texto del titulo"


### 3.3 - Comandos "sync"

Ejecutan comandos rsync configurados en archivos sync. Devuelven OK o ERROR.

Sintaxis:

>       sync {parametros opcionales} archivo

### 3.4 - Comandos "shell"

Ejecutan comandos en la consola linux. Devuelven OK o ERROR.

Sintaxis:

>       shell {parametros opcionales} comando [argumentos]

Ejemplos:

>       shell ls -l
>
>       shell --ifok --text="Creando directorio" mkdir -p "/ruta/a/directorio"
>
>       shell --iferrorexit --errortext="Sin internet" ping 8.8.8.8 -c4
>
>       shell okcodes=1,10 --oktext="Se muestra siempre" exit 10

El primer comando lista el directorio actual.

El segundo crea un directorio si el ultimo comando resultó OK, figura siempre en
el resumen.

El tercero ejecuta un comando ping y si falla se muestra el mensaje e interrumpe
la ejecucion.

Por ultimo el cuarto se considera OK ya que el codigo de salida 10 figura en
--okcodes.

Notas:

- Tener en cuenta que el comando "exit" de linux no interrumpe la ejecucion de
  los comandos por lo que puede ser utilizado para forzar el resultado
  (OK o ERROR) de los mismos.

-----------------------------------
## 4 - FORMATO DE LOS ARCHIVOS SYNC
-----------------------------------

### 4.1 - VARIABLES

#### 4.1.1 - Variables intrinsecas y de usuario

(idem archivos rbackup)

### 4.2 - SECCIONES

#### 4.2.1 - Seccion [paths]

(requerida) Contiene los paths de origen, destino y otros opcionales.

Items:

        source: origen_de_archivos_del_comando_rsync        (requerido)
        dest: destino_de_la_copia_de_archivos               (requerido)
        logfile: archivo_donde_guardar_log_rsync            (opcional)
        backdir: directorio_de_backup_incremental           (opcional)

#### 4.2.2 Seccion [source]          

(opcional) Contiene parametros adicionales de origen de los archivos.

Se utiliza por ejemplo si los archivos origen se encuentran en un host remoto.

Items:

        shell: interprete_de_comandos_en_origen             (opcional)
        host: host_origen                                   (opcional)
        port: puerto_de_escucha_en_host_origen              (opcional)
        user: usuario_para_acceder_al_host_origen           (opcional)

Notas:

- Si la copia es local no hace falta declarar la seccion.
- Con estos parametros se genera (por ejemplo) "ssh -p22 user@x.x.x.x" para
  ejecutar rsync contra un host remoto conectando con ssh.

#### 4.2.3 - Seccion [filters]

(opcional) Contiene patrones de archivos y carpetas a incluir o excluir durante
la copia.

Se escribe un patron por linea, que luego son convertidos a parametros --exclude
o --include segun se indique y respetando el orden en que fueron declarados.

Los patrones a incluir se declaran anteponiendo el signo "+" y los patrones a
excluir anteponiendo el signo "-".

Ejemplo:

>       [filters]
>       - *.tmp
>       + /userfiles/***
>       - *

Se traducira en

```
    --exclude='*.tmp' --include='/userfiles/***' --exclude='*'
```

Notas:

- El orden en que son declarados los filtros afecta el resultado (consultar
parametros --include y --exclude en manual de rsync)

#### 4.2.4 - Seccion [options]

(opcional) Contiene todas las opciones que se enviaran al comando rsync tal cual
son leidas en el archivo, ejemplos: --verbose, --compress, --delete, ...

Al igual que en el caso de [filters] se debe escribir una por linea

---
